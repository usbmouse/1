﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            Box test = new Box(1,2,3);
            Box test1 = new Box(9, 10, 11);

            //Console.WriteLine(test);
            //Console.ReadKey();
        }
    }
    class Box
    {
        
      

        public Box(double width, double length, double height)
        {

            Width = width;
            Height = height;
            Length = length;
            
           
        }


        public double Volume
        {
            get
            {
                return Width * Length * Height;
            }
        }

        public double Space
        {
            get
            {
                return 2 * (Width * Length) + 2 * (Height * Length) + 2 * (Width * Height);
            }
        }


        public override string ToString()
        {
            return String.Format("Box({0}, {1}, {2}, Volume = {3})", Width, Height, Length, Volume);
        }
       
        public double Width { get; set; }
        public double Length { get; set; }
        public double Height { get; set; }
       
    }



}
