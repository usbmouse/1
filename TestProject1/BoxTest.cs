﻿using test;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestProject1
{
    
    
    /// <summary>
    ///Это класс теста для BoxTest, в котором должны
    ///находиться все модульные тесты BoxTest
    ///</summary>
    [TestClass()]
    public class BoxTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Тест для Volume
        ///</summary>
        [TestMethod()]
        public void VolumeTest()
        {
            double width = 1F; // TODO: инициализация подходящего значения
            double length = 2F; // TODO: инициализация подходящего значения
            double height = 3F; // TODO: инициализация подходящего значения

            
            Box target = new Box(width, length, height); // TODO: инициализация подходящего значения
            double actual;
            actual = target.Volume;
            double expected;
            expected = 6F;
            Assert.AreEqual(expected, actual);
            
            
        }

        /// <summary>
        ///Тест для Width
        ///</summary>
        [TestMethod()]
        public void WidthTest()
        {
            double width = 9F; // TODO: инициализация подходящего значения
            double length = 10F; // TODO: инициализация подходящего значения
            double height = 11F; // TODO: инициализация подходящего значения
            Box target = new Box(width, length, height); // TODO: инициализация подходящего значения
            double expected = 9F; // TODO: инициализация подходящего значения
            double actual;
           
            actual = target.Width;
            Assert.AreEqual(expected, actual);
           
        }

        /// <summary>
        ///Тест для Height
        ///</summary>
        [TestMethod()]
        public void HeightTest()
        {
            double width = 1F; // TODO: инициализация подходящего значения
            double length = 0F; // TODO: инициализация подходящего значения
            double height = 1F; // TODO: инициализация подходящего значения
            Box target = new Box(width, length, height); // TODO: инициализация подходящего значения
            double expected = 1F; // TODO: инициализация подходящего значения
            double actual;
            
            actual = target.Height;
            Assert.AreEqual(expected, actual);
           
        }

        /// <summary>
        ///Тест для Space
        ///</summary>
        [TestMethod()]
        public void SpaceTest()
        {
            double width = 1F; // TODO: инициализация подходящего значения
            double length = 2F; // TODO: инициализация подходящего значения
            double height = 3F; // TODO: инициализация подходящего значения
            Box target = new Box(width, length, height); // TODO: инициализация подходящего значения
            double actual;
            actual = target.Space;
            double expected = 22F;

            Assert.AreEqual(expected, actual);
           
        }

        /// <summary>
        ///Тест для Length
        ///</summary>
        [TestMethod()]
        public void LengthTest()
        {
            double width = 2F; // TODO: инициализация подходящего значения
            double length = 1F; // TODO: инициализация подходящего значения
            double height = 3F; // TODO: инициализация подходящего значения
            Box target = new Box(width, length, height); // TODO: инициализация подходящего значения
            double expected = 1F; // TODO: инициализация подходящего значения
            double actual;
           
            actual = target.Length;
            Assert.AreEqual(expected, actual);
            
        }
    }
}
